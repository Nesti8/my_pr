#ifndef _TABLE_RW_H__
#define _TABLE_RW_H__

struct clinic {
	int id;
	char name[255];
	char address[255];
};

void read_clinic(const char *src, struct clinic *dst);
void write_clinic(const struct clinic *src, char *dst);

#endif // _TABLE_RW_H__
