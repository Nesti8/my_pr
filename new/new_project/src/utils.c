#include <stdlib.h>
#include <string.h>

#include "utils.h"


int write_to_base(Data *base) {
    puts("1 Number account: ");
    char str[30] = "",
         *s = NULL;
    int key=1;
    s = fgets(str, sizeof(str), stdin);
        while (s != NULL) {
            s[strlen(s)-1]='\0';

            switch (key) {
                case 1: base->number = strtol(str, NULL, 10);
                   puts("2 Client name: ");
                   break;
                case 2: strncpy(base->name, s, 30);
                   puts("3 Surname: ");
                   break;
                case 3: strncpy(base->surname, s, 30);
                   puts("4 Address client: ");
                   break;
                case 4: strncpy(base->address, s, 30);
                   puts("5 Client Telnumber: ");
                   break;
                case 5: strncpy(base->telnumber, s, 30);
                   puts("6 Client indebtedness: ");
                   break;
                case 6: base->indebtedness = strtod(str, NULL);
                   puts("7 Client credit limit: ");
                   break;
                case 7: base->credit_limit = strtod(str, NULL);
                   puts("8 Client cash payments: ");
                   break;
                case 8: base->cash_payments = strtod(str, NULL);
                   break;
            }
            
            if (key == 8) {
                return 0;
            } else {
                s = fgets(str, sizeof(str), stdin);
                key += 1;
            }
    }
    return 1;
}

void read_base(Data *base) {
     printf("%s%d\n%s%s\n%s%s\n%s%s\n%s%s\n%s%f\n%s%f\n%s%f\n\n",
                                                    "1 Number account: ", base->number,
                                                    "2 Client name: ", base->name,
                                                    "3 Surname: ", base->surname,
                                                    "4 Address client: ", base->address,
                                                    "5 Client Telnum: ", base->telnumber,
                                                    "6 Client indebtedness: ", base->indebtedness,
                                                    "7 Client credit limit: ", base->credit_limit,
                                                    "8 Client cash payments: ", base->cash_payments);

}

int write_to_file(const char *file,Data *client, int key) {
    FILE *f = fopen(file, "a");
    
    if (f == NULL) {
        return 1;
    } else {
        if (key == 1) {
            fprintf(f, "%-12d%-11s%-11s%-16s%20s%12.2f%12.2f%12.2f\n",
                                                    client->number, client->name,
                                                    client->surname, client->address,
                                                    client->telnumber, client->indebtedness,
                                                    client->credit_limit, client->cash_payments);
        } else {
            fprintf(f, "%-3d %-6.2f\n", client->number, client->cash_payments);
        }

        fclose(f);

    }
    return 0;
}

int read_from_file(const char *file, Data *client, int key) {
    FILE *f = fopen(file, "r");
    int res = 0, n = 0;

    if (key == 1) {
        res = fscanf(f, "%12d%11s%11s%16s%20s%12lf%12lf%12lf",
                                       &client->number, client->name,
                                       client->surname, client->address,
                                       client->telnumber, &client->indebtedness,
                                       &client->credit_limit, &client->cash_payments); 
        n=8;
    } else {
        res = fscanf(f, "%12d%12lf", &client->number, &client->cash_payments);
        n = 2;
    }
    if (res < n && res > -1) {
        while (fgetc(f)!='\n') {
            ;
        }
    res=1;
    } else {
        res=-1;
    }

    fclose(f);
    return res;
    }
