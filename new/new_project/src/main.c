#include "utils.h"

int main(void) {
    Data base = {0, "", "", "", "", 0., 0., 0.};
    write_to_base(&base);
    read_base(&base);
    write_to_file("record.dat", &base, 2);
    read_from_file("record.dat", &base, 2);
    read_base(&base);
    return 0;   
}
