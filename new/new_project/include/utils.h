#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>
    
struct masterRecord {
        int number;
        char name[20];
        char surname[20];
        char address[30];
        char telnumber[15];
        double indebtedness;
        double credit_limit;
        double cash_payments;
};


typedef struct masterRecord Data;

int write_to_base(Data *base);
void read_base(Data *base); 
int write_to_file(const char *file, Data *base, int key);
int read_from_file(const char *file, Data *base, int key);

#endif  // UTIL_H

