#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "table.h"

void read_clinic(const char *src, struct clinic *dst) {
	//<id>|<name>|<address>
	dst->id = atoi(src);
		
	char *delim = strchr(src, '|');
	char *next_delim = strchr(delim + 1, '|');
	sprintf(dst->name, "%.*s", (int)(next_delim - delim - 1), delim+1);
	sprintf(dst->address, "%s", next_delim + 1);
}

void write_clinic(const struct clinic *src, char *dst) {
	sprintf(dst, "%d|%s|%s", src->id, src->name, src->address);
}
