#include "console_rw.h"
#include "table.h"

#include <stdio.h>

int main() {
	char *input = "123|name|address";
	struct clinic cl={};
	read_clinic(input, &cl);
	printf("%d\n%s\n%s\n", cl.id, cl.name, cl.address);

	char output[512]="";
	write_clinic(&cl,output);
	printf("%s\n", output);

	return 0;
}
